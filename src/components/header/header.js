import { Grid, Button } from "@mui/material";

const Header = () =>{
    return(
        <>
            <Grid className="header"  container textAlign="center">
                <Grid item xs={3}>
                    <Button variant="" className="headerButton"><h4>Home</h4></Button>
                </Grid>
                <Grid item xs={3}>
                    <Button variant="" className="headerButton"><h4>Combo</h4></Button>
                </Grid>
                <Grid item xs={3}>
                    <Button variant="" className="headerButton"><h4>Loại Pizza</h4></Button>
                </Grid>
                <Grid item xs={3}>
                    <Button variant="" className="headerButton"><h4>Gửi Đơn Hàng</h4></Button>
                </Grid>
            </Grid>
        </>
    )
}
export default Header;