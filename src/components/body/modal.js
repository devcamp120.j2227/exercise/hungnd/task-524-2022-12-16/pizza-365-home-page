import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useState } from 'react';
import { useSelector, useDispatch } from "react-redux";
import { SendOrderHandler } from '../../actions/task.action';

//sử dụng async await
const fetchApi = async (url, body) => {
    const response = await fetch(url, body);

    const data = await response.json();
    console.log(data)
    return data;
}

const AlertDialog =() =>{
    //khai báo dispatch
    const dispatch = useDispatch();

    const [open, setOpen] = useState(true);

    const handleClose = () => {
        setOpen(false);
    };
    const {name, order, bodyForm} = useSelector((reduxData)=>{
        return reduxData.taskReducer;
    });
    const body ={
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        redirect: 'follow'
        },
        body: JSON.stringify({
            fullName: bodyForm.fullName,
            email: bodyForm.email,
            address: bodyForm.address,
            phone: bodyForm.phone,
            voucher: bodyForm.voucher,
            message: bodyForm.message,
            order: order
        }),
    }
   
    const onBtnSendOrderHandler = ()=>{
            fetchApi("http://localhost:8000/api/users", body)
                .then((data) =>{
                    console.log(data);
                dispatch(SendOrderHandler( data.data._id));
                })
                .catch((error)=>{
                    console.log(error.message)
                })
            setOpen(false);
            }
       // 
    return (
        <div>
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
            {`Order Confirm From Customer ${name}`}
            </DialogTitle>
            <DialogContent>
            <DialogContentText id="alert-dialog-description">
                    Xác nhận gửi đơn hàng?
            </DialogContentText>
            </DialogContent>
            <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={onBtnSendOrderHandler} autoFocus>
                Send
            </Button>
            </DialogActions>
        </Dialog>
        </div>
  );
}
export default AlertDialog;