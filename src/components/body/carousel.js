import img1 from "../../assets/Images/1.jpg"
import img2 from "../../assets/Images/2.jpg"
import img3 from "../../assets/Images/3.jpg"
import img4 from "../../assets/Images/4.jpg"
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import { Container } from "@mui/system";
import { Grid } from "@mui/material";
const SliderCaroul = () =>{
    const settings = {
      dots: true,
      fade: true,
      autoplay: true,
      speed: 1000,
      autoplaySpeed: 2000,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
        <Container>
            <Grid className="brand">
                <h2>PIZZA 365</h2>
                <p><i>Truly Italy!</i></p>
            </Grid>
            <Slider {...settings}>
            <div>
                <img className="ImgCasroul" src={img1} alt="1"/>
                <div className="text-img" >
                <h3>TUNA PIZZA</h3>
                </div>
            </div>
            <div >
                <img className="ImgCasroul" src={img4} alt="2" />
                <div className="text-img" >
                    <h3> OYSTER PIZZA</h3>
                </div>
            </div>
            <div>
                <img src={img2} className="ImgCasroul" alt="3"/>
                <div className="text-img" >
                    <h3>SEAFOOD PIZZA</h3>
                </div>
            </div>
            <div>
                <img src={img3} className="ImgCasroul" alt="4"/>
                <div className="text-img" >
                    <h3>TROPICAL PIZZA</h3>
                </div>
            </div>
            </Slider>
        </Container>
    );
  }
  export default SliderCaroul;