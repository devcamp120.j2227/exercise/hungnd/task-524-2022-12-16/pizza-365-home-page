import { Grid, Card, CardHeader, CardContent, Typography, CardActions } from "@mui/material";
import { Container } from "@mui/system";
import { useDispatch, useSelector } from "react-redux";
import ButtonStyle from "../styleComponent/styleButton";
import { PizzaTypeHandler } from "../../actions/task.action";

const PizzaType = () =>{
    //Khai báo dispatch để lấy giá trị
    const dispatch = useDispatch()
    //B1: nhận giá trị khởi tạo của state trong giai đoạn đầu
    const {pizzaType} = useSelector((reduxData)=>{
        return reduxData.taskReducer;
    });
    const onButtonPizzaType = (id) =>{
        dispatch(PizzaTypeHandler(id));
    }
    return(
        <Container>
            <Grid   container 
                    justifyContent="center" 
                    marginTop="50px" 
                    marginBottom="20px" 
                    direction="column" 
                    alignItems="center">
                <Grid item className="brand">
                    <h2>
                        CHỌN LOẠI PIZZA
                    </h2>
                    <hr className="title-hr"/>
                </Grid>
            </Grid>
            <Grid container spacing={10} marginBottom="20px">
            {pizzaType.map((value,index)=>{
                return (
                <Grid item xs={4} key={index}>
                    <Card p={0}>
                        <CardHeader sx={{p:0}}
                                subheader={
                                <CardActions sx={{p:0}}>
                                    <img className="imgPizza" src={require("../../assets/Images/"+ value.image)}/>
                                </CardActions>
                            }/>
                            <CardContent sx={{height:260}}>
                                {
                                <Typography variant="body">
                                    <Grid
                                    container className="pizza-decrip"
                                    direction="column"
                                    justifyContent="center"
                                    alignItems="center"
                                    textAlign="center"
                                    >
                                        <h2>{value.pizzaType}</h2>
                                        <p><b>{value.title}</b></p>
                                        <p>{value.decription}</p>
                                    </Grid>
                                </Typography>
                                }
                            </CardContent>
                            <CardHeader className="footer-card"
                                subheader={
                                <CardActions>
                                    <Grid 
                                    container
                                    direction="row"
                                    justifyContent="center"
                                    alignItems="center">
                                        <ButtonStyle variant="contained" size="medium" onClick={()=> onButtonPizzaType(index)}>Chọn</ButtonStyle>
                                    </Grid>
                                </CardActions>
                            }/>
                        </Card>
                </Grid>
                ) })}
            </Grid>
        </Container>
    )
}
export default PizzaType;