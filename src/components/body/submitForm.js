import { Grid, Box, TextField } from "@mui/material";
import { Container } from "@mui/system";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FormHandler, SubmitFormHandler } from "../../actions/task.action";
import ButtonSubmitStyle from "../styleComponent/styleButtonSubmitForm";
import AlertDialog from "./modal";
const SubmitForm = () =>{
    const {status} = useSelector((reduxData)=>{
        return reduxData.taskReducer;
    });
    //Khai báo dispatch để lấy giá trị
    const dispatch = useDispatch();
    const [inputValue, setValue] = useState({
        fullName:"",
        email:"",
        phone:"",
        address:"",
        voucher:"",
        message:""
    });
    const handleChange= (event) =>{
        const value = event.target.value;
        setValue({
            ...inputValue,
            [event.target.name]: value
        });
        dispatch(FormHandler({
            ...inputValue,
            [event.target.name]: value
        }))
    }
    const onBtnSendFormHandler = () =>{
        dispatch(SubmitFormHandler())
    }
    return(
        <Container>
             <Grid container 
                    justifyContent="center" 
                    marginTop="50px" 
                    marginBottom="20px" 
                    direction="column" 
                    alignItems="center">
                <Grid item className="brand">
                    <h2>
                        GỬI ĐƠN HÀNG
                    </h2>
                    <hr className="title-hr"/>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item> <p>Tên</p></Grid>
                <Box sx={{width: "100%"}}>
                    <TextField fullWidth label="Nhập tên" name="fullName" value={inputValue.fullName} onChange={handleChange}/>
                </Box>
                <Grid item> <p>Email</p></Grid>
                <Box sx={{width: "100%"}}>
                    <TextField fullWidth label="Nhập email" name="email" value={inputValue.email} onChange={handleChange}/>
                </Box>
                <Grid item> <p>Điện thoại</p></Grid>
                <Box sx={{width: "100%"}}>
                    <TextField fullWidth label="Nhập số điện thoại" name="phone" value={inputValue.phone} onChange={handleChange}/>
                </Box>
                <Grid item> <p>Địa chỉ</p></Grid>
                <Box sx={{width: "100%"}}>
                    <TextField fullWidth label="Nhập địa chỉ" name="address" value={inputValue.address} onChange={handleChange}/>
                </Box>
                <Grid item> <p>Mã giảm giá</p></Grid>
                <Box sx={{width: "100%"}}>
                    <TextField fullWidth label="Nhập mã giảm giá" name="voucher" value={inputValue.voucher} onChange={handleChange}/>
                </Box>
                <Grid item> <p>Lời nhắn</p></Grid>
                <Box sx={{width: "100%"}}>
                    <TextField fullWidth label="Nhập lời nhắn" name="message" value={inputValue.message} onChange={handleChange}/>
                </Box>
                <br/>
                <ButtonSubmitStyle fullWidth onClick={onBtnSendFormHandler}>Gửi</ButtonSubmitStyle>
                {status ===true?<AlertDialog/>:null}
            </Grid>
        </Container>
    )
}
export default SubmitForm;